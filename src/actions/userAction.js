import {USER_LOGIN_SUCCESS} from "../constants/userConstants";
import data from './../data/db.json'
import { saveAuth } from "../services/userServices";
import { saveToDo } from "../services/todoServices";
export const login = (name) => async (dispatch) => {
    try {
        //check name
        var selectedData = data.find(d => {
            return d.name == name
        })
        if(selectedData){
            saveAuth(selectedData.name)
            saveToDo(selectedData.todo)
            let payload = {
                loading:false, 
                userInfo: selectedData.name
            }
            dispatch({
                type: USER_LOGIN_SUCCESS,
                payload
            })
        }
       
    } catch (error) {
       console.log("error",error)
    }
}


export const logout = () => async (dispatch) => {
    localStorage.clear()
}



