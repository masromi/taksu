import { Form, Button } from 'react-bootstrap'
import data from './../data/db.json'
import { useDispatch, useSelector } from 'react-redux'
import { login } from './../actions/userAction'
import { useState } from 'react'
const Login = () => {

    const [name, setName] = useState()
    const dispatch = useDispatch()
    const checklogin = async () => {
        await dispatch(login(name))
    }
    return (
        <div className="login">
            <div className="login-form">
                <Form >
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="" value={name} onChange={($e) => setName($e.target.value)} />
                    </Form.Group>
                    <Button className="btn btn-primary" type="submit" onClick={() => checklogin()}>
                        Next
                    </Button>
                </Form>
            </div>
        </div>
    )
}


export default Login